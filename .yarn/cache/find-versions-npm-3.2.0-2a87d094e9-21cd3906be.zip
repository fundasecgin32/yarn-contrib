PK       !                node_modules/PK       !                node_modules/find-versions/PK
       ! �E�}U  U  "   node_modules/find-versions/licenseMIT License

Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
PK
       ! c���*  *  #   node_modules/find-versions/index.js'use strict';
const semverRegex = require('semver-regex');

module.exports = (stringWithVersions, options = {}) => {
	if (typeof stringWithVersions !== 'string') {
		throw new TypeError(`Expected a string, got ${typeof stringWithVersions}`);
	}

	const reLoose = new RegExp(`(?:${semverRegex().source})|(?:v?(?:\\d+\\.\\d+)(?:\\.\\d+)?)`, 'g');
	const matches = stringWithVersions.match(options.loose === true ? reLoose : semverRegex()) || [];

	return [...new Set(matches.map(match => match.trim().replace(/^v/, '').replace(/^\d+\.\d+$/, '$&.0')))];
};
PK
       ! U�  �  '   node_modules/find-versions/package.json{
	"name": "find-versions",
	"version": "3.2.0",
	"description": "Find semver versions in a string: `unicorn v1.2.3` → `1.2.3`",
	"license": "MIT",
	"repository": "sindresorhus/find-versions",
	"author": {
		"name": "Sindre Sorhus",
		"email": "sindresorhus@gmail.com",
		"url": "sindresorhus.com"
	},
	"engines": {
		"node": ">=6"
	},
	"scripts": {
		"test": "xo && ava && tsd"
	},
	"files": [
		"index.js",
		"index.d.ts"
	],
	"keywords": [
		"semver",
		"version",
		"versions",
		"regex",
		"regexp",
		"match",
		"matching",
		"semantic",
		"find",
		"extract",
		"get"
	],
	"dependencies": {
		"semver-regex": "^2.0.0"
	},
	"devDependencies": {
		"ava": "^1.4.1",
		"tsd": "^0.7.2",
		"xo": "^0.24.0"
	}
}
PK
       ! �f�@�  �  $   node_modules/find-versions/readme.md# find-versions [![Build Status](https://travis-ci.com/sindresorhus/find-versions.svg?branch=master)](https://travis-ci.com/sindresorhus/find-versions)

> Find semver versions in a string: `unicorn v1.2.3` → `1.2.3`


## Install

```
$ npm install find-versions
```


## Usage

```js
const findVersions = require('find-versions');

findVersions('unicorn v1.2.3 rainbow 2.3.4+build.1');
//=> ['1.2.3', '2.3.4+build.1']

findVersions('cp (GNU coreutils) 8.22', {loose: true});
//=> ['8.22.0']
```


## API

### findVersions(stringWithVersions, [options])

#### stringWithVersions

Type: `string`

#### options

Type: `Object`

##### loose

Type: `boolean`
Default: `false`

Also match non-semver versions like `1.88`. They're coerced into semver compliant versions.


## Related

- [find-versions-cli](https://github.com/sindresorhus/find-versions-cli) - CLI for this module


## License

MIT © [Sindre Sorhus](https://sindresorhus.com)
PK
       ! n��g}  }  %   node_modules/find-versions/index.d.tsdeclare namespace findVersions {
	interface Options {
		/**
		Also match non-semver versions like `1.88`. They're coerced into semver compliant versions.

		@default false
		*/
		readonly loose?: boolean;
	}
}

/**
Find semver versions in a string: `unicorn v1.2.3` → `1.2.3`.

@example
```
import findVersions = require('find-versions');

findVersions('unicorn v1.2.3 rainbow 2.3.4+build.1');
//=> ['1.2.3', '2.3.4+build.1']

findVersions('cp (GNU coreutils) 8.22', {loose: true});
//=> ['8.22.0']
```
*/
declare function findVersions(
	stringWithVersions: string,
	options?: findVersions.Options
): string[];

export = findVersions;
PK?       !                        �A    node_modules/PK?       !                        �A+   node_modules/find-versions/PK?
       ! �E�}U  U  "           ��d   node_modules/find-versions/licensePK?
       ! c���*  *  #           ���  node_modules/find-versions/index.jsPK?
       ! U�  �  '           ��d  node_modules/find-versions/package.jsonPK?
       ! �f�@�  �  $           ��s
  node_modules/find-versions/readme.mdPK?
       ! n��g}  }  %           ��_  node_modules/find-versions/index.d.tsPK            